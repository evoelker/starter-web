# Starter Web Project

This is a simple website project for showing how to use Git and GitHub together. Adding a line for rebasing example, more changes from myfeature branch too. Updating for emergency fix after stashing.

## Introduction

This example is to show different parts of the Git repository and various commands using a web project.

## Purpose

As stated above, the main purpose is to provide simple examples for git training demos.

## Deployment

This is a simple web project, deployment can be on any web server or even local file system.

## How To Contribute

Please fork this repository and then issue Paull Request for review.

### Copyright

Coypright 2017 Git.Training. All rights reserved.
